package com.kaushikam.constructor

fun main() {
    val student = Student("kaushik asokan")
    // student.name is not accessible outside

    val teacher = Teacher("Ashif Ubaidullah")
    println("Teacher ${teacher.name} is a good teacher")

    val attender = Attender()
    println("Attender's name is ${attender.name}")
}

class Student constructor (name: String/*known as parameters*/) {
    init {
        println("Name of the student is $name")
    }
}

class Teacher (val name: String) {
    init {
        println("Name of the teacher is $name")
    }
}

class Attender constructor (pName: String) {
    var name: String = ""
    var salary: Long = -1

    init {
        println ("This will print salary as $salary, because init block runs just after primary constructor")
    }

    constructor(pName: String, pSalary: Long): this(pName) {
        this.salary = salary
    }

    constructor(): this("")
}

class School() {
    var name: String = ""
        private set

    var address: String = ""
        private set

    constructor(name: String, address: String): this() {
        this.name = name
        this.address = address
    }
}


